const express = require('express')
const path = require('path');
const PORT = process.env.PORT || 9999;
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));

if (process.env.NODE_ENV === 'production') {
  app.use(express.static(path.join(__dirname, "project/build")));
  app.get("*", (_, res) => {
    res.sendFile(path.join(__dirname, "project/build", "index.html"));
  });
}

app.use((_res) => {
  return res.status(404).send("404 Not Found");
});

app.listen(PORT, () => console.log(`Listening on ${PORT}`));